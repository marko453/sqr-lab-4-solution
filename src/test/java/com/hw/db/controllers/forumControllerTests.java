package com.hw.db.controllers;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Forum;
import com.hw.db.models.Message;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

class forumControllerTests {
    private User loggedIn;
    private Forum toCreate;

    @BeforeEach
    @DisplayName("Forum Creation")
    void createForumTest() {
        loggedIn = new User("some", "some@email.mu", "name", "nothing");
        toCreate = new Forum(12, "some", 3, "title", "some");
    }

    @Test
    @DisplayName("Forum Creation Test")
    void correctlyCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(toCreate), controller.create(toCreate),
                        "Result for succeeding forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("User not authorized")
    void noLoginWhenCreatesForumAlreadyExists() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenThrow(new EmptyResultDataAccessException("User not found.", 0));
            forumController controller = new forumController();
            controller.create(toCreate);
            assertEquals(ResponseEntity.status(HttpStatus.NOT_FOUND).build().getStatusCode(),
                    controller.create(toCreate).getStatusCode(),
                    "Result when unauthorised user tried to create a forum");
        }
    }

    @Test
    @DisplayName("Forum already exists")
    void conflictWhenCreatesForumNoAccess() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate))
                        .thenThrow(new DuplicateKeyException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).build(), controller.create(toCreate),
                        "Result for conflict while forum creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }

    @Test
    @DisplayName("Server cannot access database")
    void conflictWhenCreatesForum() {
        try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
            userMock.when(() -> UserDAO.Search("some"))
                    .thenReturn(loggedIn);
            try (MockedStatic forumDAO = Mockito.mockStatic(ForumDAO.class)) {
                forumDAO.when(() -> ForumDAO.CreateForum(toCreate))
                        .thenThrow(new DuplicateKeyException("Forum already exists"));
                forumController controller = new forumController();
                controller.create(toCreate);
                assertEquals(ResponseEntity.status(HttpStatus.CONFLICT).build(),
                        controller.create(toCreate), "Result for DB error while server creation");
            }
            assertEquals(loggedIn, UserDAO.Search("some"));
        }
    }
}
