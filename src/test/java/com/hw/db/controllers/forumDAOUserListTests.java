package com.hw.db.controllers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class forumDAOUserListTests {
    @Test
    @DisplayName("SQL: All NULLs")
    void allOptionalParametersAreNull() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        ForumDAO.UserList("", null, null, null);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;"),
                Mockito.eq(new Object[] { "" }), Mockito.any(UserDAO.UserMapper.class));
    }
    @Test
    @DisplayName("SQL: Condition, DESC, LIMIT")
    void sinceIsNotNullAndDescIsTrueAndLimitIsNotNull() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        ForumDAO.UserList("", 1, "", true);
        verify(mockJdbc).query(Mockito.eq(
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                Mockito.eq(new Object[] { "", "", 1}), Mockito.any(UserDAO.UserMapper.class));
    }
    @Test
    @DisplayName("SQL: Comparison Condition")
    void sinceIsNotNullAndDescNull() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ForumDAO(mockJdbc);
        ForumDAO.UserList("", null, "", null);
        verify(mockJdbc).query(Mockito.eq(
            "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                Mockito.eq(new Object[] { "", ""}), Mockito.any(UserDAO.UserMapper.class));
    }
}