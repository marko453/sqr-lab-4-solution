package com.hw.db.controllers;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import javax.swing.tree.RowMapper;
import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

public class threadDAOTreeSortTests {

    @Test
    @DisplayName("SQL: Passing Since")
    void passSinceAndSqlShouldContainComparison() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(0, 1, 5, null);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.eq(0), Mockito.eq(5), Mockito.eq(1));
    }

    @Test
    @DisplayName("SQL: Passing Since and DESC")
    void passSinceAndDescTrueSqlShouldContainDesc() {
        JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
        ThreadDAO.treeSort(0, 1, 5, true);
        verify(mockJdbc).query(Mockito.eq(
                "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Mockito.any(PostDAO.PostMapper.class), Mockito.eq(0), Mockito.eq(5), Mockito.eq(1));
    }
}